function getTopTenPlayers(wcPlayersData) {
  const eventWithG = wcPlayersData.filter((data) => data.Event.includes("G"));
  let goalsCount = eventWithG.reduce((acc, curr) => {
    let goals = 0;
    let event = curr.Event.split(" ");
    event.forEach((element) => {
      if (element.includes("G")) goals++;
      if (element.includes("OG")) goals--;
    });
    acc[curr["Player Name"]]
      ? (acc[curr["Player Name"]] += goals)
      : (acc[curr["Player Name"]] = goals);
    return acc;
  }, {});

  const matchesPlayed = wcPlayersData.reduce((acc, curr) => {
    acc[curr["Player Name"]]
      ? acc[curr["Player Name"]]++
      : (acc[curr["Player Name"]] = 1);
    return acc;
  }, {});
  let probability = Object.keys(matchesPlayed).reduce((acc, key) => {
    if (key in goalsCount) {
      acc[key] = goalsCount[key] / matchesPlayed[key];
    }
    return acc;
  }, {});
  let sortedOutput = Object.entries(probability)
    .sort((a, b) => b[1] - a[1])
    .slice(0, 10);
  let playersWithHighProbability = Object.fromEntries(sortedOutput);
  return playersWithHighProbability;
}
module.exports = getTopTenPlayers;
