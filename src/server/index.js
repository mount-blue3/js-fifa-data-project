const csv = require("csvtojson");
const fs = require("fs");
const getMatchesPerCity = require("../server/1-matches-per-year.cjs");
const getMatchesWonPerTeam = require("../server/2-matches-won-per-team-per-year.cjs");
const getRedCardsPerTeam = require("../server/3-redcards-per-team-2014.cjs");
const getTopTenPlayers = require("../server/4-top10Players.cjs");
const WorldCupMatchesPath = "../data/WorldCupMatches.csv";
const WorldCupPlayersPath = "../data/WorldCupPlayers.csv";
const WorldCupsPath = "../data/WorldCups.csv";
const jsonPath1 = "../public/output/1-matches-per-year.json";
const jsonPath2 = "../public/output/2-matches-won-per-team-per-year.json";
const jsonPath3 = "../public/output/3-redcards-per-team-2014.json";
const jsonPath4 = "../public/output/4-top-10-players.json";
const toWriteFile = (filePath, res) => {
  fs.writeFile(filePath, JSON.stringify(res), (err) => {
    if (err) {
      console.log(err);
    }
  });
};
const forMatchesPerCity = (filePath) => {
  csv()
    .fromFile(filePath)
    .then((data) => {
      let res = getMatchesPerCity(data);
      console.log(res);
      toWriteFile(jsonPath1, res);
    });
};
const forMatchesWonPerTeam = (filePath) => {
  csv()
    .fromFile(filePath)
    .then((data) => {
      let res = getMatchesWonPerTeam(data);
      console.log(res);
      toWriteFile(jsonPath2, res);
    });
};
const forRedCardsPerTeam = (filePathForMatches, filePathForPlayers, year) => {
  csv()
    .fromFile(filePathForMatches)
    .then((matches) => {
      csv()
        .fromFile(filePathForPlayers)
        .then((players) => {
          let res = getRedCardsPerTeam(matches, players, year);
          console.log(res);
          toWriteFile(jsonPath3, res);
        });
    });
};
const forTopTenPlayers = (filePath) => {
  csv()
    .fromFile(filePath)
    .then((data) => {
      let res = getTopTenPlayers(data);
      console.log(res);
      toWriteFile(jsonPath4, res);
    });
};
forMatchesPerCity(WorldCupMatchesPath);
forMatchesWonPerTeam(WorldCupMatchesPath);
forRedCardsPerTeam(WorldCupMatchesPath, WorldCupPlayersPath, "2014");
forTopTenPlayers(WorldCupPlayersPath);
