function getMatchesPerCity(matches) {
  //Getting count of cities
  const matchesPerCity = matches.reduce((acc, curr) => {
    //Check City has value
    if (curr["City"] !== "") {
      acc[curr["City"]] ? acc[curr["City"]]++ : (acc[curr["City"]] = 1);
    }
    return acc;
  }, {});
  return matchesPerCity;
}
module.exports = getMatchesPerCity;
