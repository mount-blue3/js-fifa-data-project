function getMatchesWonPerTeam(matches) {
  const matchesWonPerTeam = matches.reduce((acc, curr) => {
    if (curr["Home Team Name"] !== "") {
      if (curr["Home Team Goals"] > curr["Away Team Goals"]) {
        acc[curr["Home Team Name"]]
          ? acc[curr["Home Team Name"]]++
          : (acc[curr["Home Team Name"]] = 1);
      } else if (curr["Away Team Goals"] > curr["Home Team Goals"]) {
        acc[curr["Away Team Name"]]
          ? acc[curr["Away Team Name"]]++
          : (acc[curr["Away Team Name"]] = 1);
      }
    }
    return acc;
  }, {});
  return matchesWonPerTeam;
}
module.exports = getMatchesWonPerTeam;
