function getRedCardsPerTeam(matches, players, year) {
  const matchesIn2014 = matches.filter((matchyear) => matchyear.Year === year);
  const matchID = matchesIn2014.map((id) => id.MatchID);
  const eventWithRedCards = players.filter((playerData) => {
    if (matchID.includes(playerData.MatchID))
      return playerData.Event.includes("R");
  });
  const dataWithTeamIntials = eventWithRedCards.map(
    (team) => team["Team Initials"]
  );
  const redCardsPerTeam = dataWithTeamIntials.reduce((acc, curr) => {
    acc[curr] ? acc[curr]++ : (acc[curr] = 1);
    return acc;
  }, {});
  return redCardsPerTeam;
}

module.exports = getRedCardsPerTeam;
